int ldrPin = A0;
int ldrStatus ;

void setup() {

Serial.begin(9600);
pinMode(ldrPin, INPUT);

}

void loop() {

ldrStatus = analogRead(ldrPin);

  if (ldrStatus < 550) {
  Serial.print("Cahaya Sangat Gelap: ");
  Serial.println(ldrStatus);
  delay(500);
  } 
    else if (ldrStatus >= 450 && ldrStatus <= 650) {
      Serial.print("Cahaya Redup: ");
      Serial.println(ldrStatus);
      delay(500);
      } 
       else if (ldrStatus >=550  && ldrStatus <= 750) {
      Serial.print("Cahaya Normal: ");
      Serial.println(ldrStatus);
      delay(500);
      } 
       else if (ldrStatus >= 650 && ldrStatus <= 850) {
      Serial.print("Cahaya Terang: ");
      Serial.println(ldrStatus);
      delay(500);
      } 
        else {
        Serial.print("Cahaya Sangat Terang: ");
        Serial.println(ldrStatus);
        delay(500);
        }

}
