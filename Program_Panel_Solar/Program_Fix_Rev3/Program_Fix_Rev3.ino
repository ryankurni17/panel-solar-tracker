#include <DS3231.h>
#include <LiquidCrystal_I2C.h>
DS3231 rtc(SDA, SCL);
LiquidCrystal_I2C lcd(0x27,20,4);

int RPWM = 11;
int LPWM = 3;
int Pin_riset = 7;
int Pin_posisi = 8;
int ldrPin = A0;
int ldrStatus;

void setup() {
lcd.init();         // inisialisasi lCD 20 x 4
lcd.backlight();    // Menghidupkan Backlight LCD
  
rtc.begin();
//rtc.setDOW(TUESDAY);        // Set Hari (Nama Hari)
//rtc.setTime(16, 38, 00);    // Set jam 12:00:00 (24hr format)
//rtc.setDate(7, 6, 2022);    // Set Tanggal, Bulan, Tahun

pinMode(RPWM,OUTPUT);       // Inisiasi output Aktuator Naik
pinMode(LPWM,OUTPUT);       // Inisiasi output Aktuator Turun
pinMode(7, INPUT_PULLUP);   // Inisiasi Input Switch Button Riset
pinMode(8, INPUT_PULLUP);   // Inisiasi Input Switch Button Posisi Aktuator
pinMode(ldrPin, INPUT);     // Inisiasi Input Sensor LDR

}

void sensor_cahaya(){
  
  ldrStatus = analogRead(ldrPin);   // Pembacaan Nilai Sensor LDR

  if (ldrStatus < 550) {    // Kondisi Nilai Sensor LDR < 550 maka Sensor membaca Cahaya Sangat Gelap
    lcd.setCursor(0,3);
    lcd.print("Cahaya Sangat Gelap");
    delay(500);
    } 
    else if (ldrStatus >= 450 && ldrStatus <= 650) {  // Kondisi Nilai Sensor LDR >= 450 dan Kurang <= 650 maka Sensor membaca Cahaya Redup
      lcd.setCursor(0,3);
      lcd.print("Cahaya Redup");
      delay(500);
    } 
    else if (ldrStatus >= 550 && ldrStatus <= 750) {  // Kondisi Nilai Sensor LDR >= 550 dan Kurang <= 750 maka Sensor membaca Cahaya Normal
      lcd.setCursor(0,3);
      lcd.print("Cahaya Normal");
      delay(500);
    } 
    else if (ldrStatus >= 650 && ldrStatus <= 850) {  // Kondisi Nilai Sensor LDR >= 650 dan Kurang <= 850 maka Sensor membaca Cahaya Terang
      lcd.setCursor(0,3);
      lcd.print("Cahaya Terang");
      delay(500);
      } 
    else {                                           // Kondisi Nilai Sensor LDR >= 850 maka Sensor membaca Cahaya Sangat Terang
        lcd.setCursor(0,3);
        lcd.print("Cahaya Sangat Terang");
        delay(500);
        }
}
 
void loop() {
lcd.setCursor(0,0);
lcd.print("'Projek Panel Surya'");
lcd.setCursor(1,1);
lcd.print(rtc.getDOWStr());   // Mengambil data nama Hari dari RTC DS3231,hasil berupa String (Bahasa English)
lcd.print(", ");
lcd.print(rtc.getDateStr());  // Mengambil data Tanggal dari RTC DS3231,hasil berupa String
lcd.setCursor(0,2);
lcd.print("Temperatur: ");
lcd.print(rtc.getTemp());     // Mengambil data suhu dari RTC DS3231, hasil berupa long int
lcd.print((char) 223);        // Membuat Simbol (º)
lcd.print("C");
sensor_cahaya();              // Memanggil Hasil Sensor LDR
delay(3000);
lcd.clear();
  
lcd.setCursor(0,0);
lcd.print("'Projek Panel Surya'");
lcd.setCursor(2,1);
lcd.print("Waktu: ");
lcd.print(rtc.getTimeStr());   // Mengambil data Waktu dari RTC DS3231,hasil berupa String
lcd.setCursor(0,2);
lcd.print("Temperatur: ");
lcd.print(rtc.getTemp());      // Mengambil data suhu dari RTC DS3231, hasil berupa long int
lcd.print((char) 223);         // Membuat Simbol (º)
lcd.print("C");
sensor_cahaya();               // Memanggil Hasil Sensor LDR
delay(10000);
lcd.clear();

if (digitalRead(7) == HIGH & digitalRead(8) == LOW){ // Pengkonsidian Switch Menentukan posisi Aktuator
  analogWrite(RPWM,255);        //  SPEED AKTUATOR MENGGUNAKAN NILAI PWM 0 - 255 (kondisi 255 On)
  analogWrite(LPWM,0);
}
  else if (digitalRead(7) == LOW & digitalRead(8) == HIGH) { // Pengkonsidian Switch Menentukan Riset Aktuator 
  analogWrite(RPWM,0);          //  SPEED AKTUATOR MENGGUNAKAN NILAI PWM 0 - 255 (kondisi 0 Off)
  analogWrite(LPWM,255);
  }
  else{
    analogWrite(RPWM,0);
    analogWrite(LPWM,0);
  }

Time t = rtc.getTime();       // Membaca waktu, Variabel t tampat menyimpan waktu

  if (t.hour == 6 && t.min == 0)  // Kondisi 1 Aktuator Bergerak Ketika jam (06.00)
  {
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(0,1);
      lcd.print("Aktuator Posisi Awal");
      delay(13000);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 52");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
  }
   else if (t.hour == 7 && t.min == 0)  // Kondisi 2 Aktuator Bergerak Ketika jam (07.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 42");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
   else if (t.hour == 8 && t.min == 0)  // Kondisi 3 Aktuator Bergerak Ketika jam (08.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 32");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 9 && t.min == 0)  // Kondisi 4 Aktuator Bergerak Ketika jam (09.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 22");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 10 && t.min == 0) // Kondisi 5 Aktuator Bergerak Ketika jam (10.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 12");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 11 && t.min == 0)  // Kondisi 6 Aktuator Bergerak Ketika jam (11.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 7");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 12 && t.min == 0)  // Kondisi 7 Aktuator Bergerak Ketika jam (12.00)
    {
     analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 178");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 13 && t.min == 0)  // Kondisi 8 Aktuator Bergerak Ketika jam (13.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 172");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 14 && t.min == 0)  // Kondisi 9 Aktuator Bergerak Ketika jam (14.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 164");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 15 && t.min == 0)  // Kondisi 10 Aktuator Bergerak Ketika jam (15.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 158");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 16 && t.min == 0)  // Kondisi 11 Aktuator Bergerak Ketika jam (16.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 150");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 17 && t.min == 0)  // Kondisi 12 Aktuator Bergerak Ketika jam (17.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 142");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 18 && t.min == 0)  // Kondisi 13 Aktuator Bergerak Ketika jam (18.00)
    {
      analogWrite(RPWM,255);
      analogWrite(LPWM,0);
      lcd.setCursor(2,1);
      lcd.print("Aktuator Berjalan");
      delay(13000);
      analogWrite(RPWM,0);
      analogWrite(LPWM,0);
      lcd.setCursor(2,2);
      lcd.print("Posisi Sudut 133");
      lcd.print((char) 223);
      delay(47000);
      lcd.clear();
    }
    else if (t.hour == 18 && t.min == 10){  // Kondisi 14 Aktuator Akan Bergerak Kembali Ke Posisi Awal
     analogWrite(RPWM,0);
     analogWrite(LPWM,255);
    }
}
